/*class mouse {
    constructor(name, weight, speed) {
        this.name = name;
        this.weight = weight;
        this.speed = speed;
        this.trang_thai = true;
        this.sounding = 'chít chít';
    }
    setSpeed(speed) {
        this.speed = speed;
    }
    setWeight(weight) {
        this.weight = weight;
    }
    setTrang_thai() {
        this.trang_thai = false;
    }
    getTrang_thai() {
        return this.trang_thai;
    }
}

class cat {
    constructor(name, weight, maxSpeed) {
        this.name = name;
        this.weight = weight;
        this.maxSpeed = maxSpeed;
        this.sounding = 'meo meo';
    }
    checkeatMouse(mouse) {
        if (mouse.trang_thai) {
            document.write("<br>" + mouse.name + " kêu: " + mouse.sounding);
            document.write("<br>" + this.name + " có thể ăn được " + mouse.name);
            this.catchMouse(mouse);
            this.eatMouse(mouse);
        } else {
            document.write("<br>" + this.name + " không ăn " + mouse.name);
        }
    }

    catchMouse(mouse) {
        if (this.maxSpeed > mouse.speed) {
            document.write("<br>" + this.name + " bắt được chuột " + mouse.name);
            document.write("<br>" + this.name + " kêu: " + this.sounding);
        } else {
            document.write("<br>" + this.name + " không bắt đc chuột " + mouse.name);
        }
    }

    eatMouse(mouse) {
        if (mouse.trang_thai && this.maxSpeed > mouse.speed) {
            this.weight = this.weight + mouse.weight;
            document.write("<br>" + this.name + " nặng lên " + this.weight);
        } else {
            document.write("<br>" + this.name + " vẫn nặng " + this.weight);
        }
    }
}

let tom = new cat('Tom', 10, 5);
let jerry = new mouse('Rat', 2, 3);
jerry.setTrang_thai();
jerry.getTrang_thai();
jerry.setSpeed(4);
jerry.setWeight(5);

tom.checkeatMouse(jerry);
*/
// vị trí //
function Hero(image, top, left, size, elm_id, dis) {
    this.image = image;
    this.top = top;
    this.left = left;
    this.size = size;
    this.dis = dis;
    this.huong_di = 'phai';
    this.elm_id = document.getElementById(elm_id);

    this.getHeroElement = function () {
        // return '<img width="' + this.size + '"' +
        //     ' height="' + this.size + '"' +
        //     ' src="' + this.image + '"' +
        //     ' style="top: ' + this.top + 'px; left:' + this.left + 'px;position:absolute;" />';

        this.elm_id.style.top = this.top + 'px';
        this.elm_id.style.left = this.left + 'px';
        this.elm_id.src = this.image;
        this.elm_id.width = this.size;
        this.elm_id.height = this.size;
    }

    this.moveRight = function () {
        //if (this.left >= (Math.random() * (1000 - 1920) + 1000) - this.size) {
        if (this.left >= window.innerWidth - this.size) {
            this.huong_di = 'xuong';
        } else {
            this.left += 20;
            this.left += this.dis;
        }

        this.getHeroElement();
    }
    this.moveLeft = function () {
        if (this.left <= 0) {
            this.huong_di = 'len';
        } else {
            this.left -= 20;
        }
        this.getHeroElement();
    }
    this.moveBot = function () {
        if (this.top >= window.innerHeight - this.size) {
            this.huong_di = 'trai';
        }
        else {
            this.top += 20;
        }
        this.getHeroElement();
    }
    this.moveTop = function () {
        if (this.top <= 0) {
            this.huong_di = 'phai';
        }
        else {
            this.top -= 20;
        }
        this.getHeroElement();
    }
}
function start(x) {
    console.log(x);
    if (x.huong_di == 'phai') {
        x.moveRight();
    } else if (x.huong_di == 'xuong') {
        x.moveBot();
    } else if (x.huong_di == 'trai') {
        x.moveLeft();
    } else if (x.huong_di == 'len') {
        x.moveTop();
    }
    //document.getElementById('game').innerHTML = x.getHeroElement();
    setTimeout(start, 100, x);
}
let Tom = new Hero('1.png', 20, 30, 200, 'cat', 0);

let Jerry = new Hero('2.png', 40, 600, 50, 'mouse', 0);

start(Tom);
start(Jerry);